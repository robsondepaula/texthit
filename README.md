# TEXTHIT

A text to hypertext conversion tool.

## Context

TEXTHIT is a free software for text to hypertext conversion designed to help on researches about text to hypertext conversion and hypertext authoring.

This work was made while earning my Master's degree.

### More information

* [Google Scholar Profile](https://scholar.google.com.br/citations?user=QucS_38AAAAJ&hl=en) - Thesis and articles.

## License

This project is licensed under the GPL v3 License - see the [COPYING.md](COPYING.md) file for details.

## Acknowledgments

* To the [Jacinth](https://sourceforge.net/projects/jacinth/) project used as a component on TEXTHIT.
